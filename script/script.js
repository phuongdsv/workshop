function solveQuadraticEquation(a, b, c) {
	var result = []

	var delta = b*b - 4*a*c
	
	if (delta === 0) {
		var x = -b/2.0/a
		result.push(x)
	} else if (delta > 0) {
		var sqrtOfDelta = Math.sqrt(delta)
		var x1 = (-b + Math.sqrt(delta))/(2.0*a)
		var x2 = (-b - Math.sqrt(delta))/(2.0*a)
		result.push(x1, x2) 
	}

	//TO DO
	return result
}

document.getElementById("solve").onclick = function(e) {
	e.preventDefault(); 

	var a, b, c
	try {
		a = parseFloat(document.getElementById("a").value)
		b = parseFloat(document.getElementById("b").value)
		c = parseFloat(document.getElementById("c").value)
	} catch(e) {
		throw e
	}
	
	if (isNaN(a) || isNaN(b) || isNaN(c) || a === 0) {
		document.getElementById("result-content").innerHTML = "Invalid input"
		return
	}

	var result = solveQuadraticEquation(a, b, c)
	var resultContent
	if (result.length === 0) {
		resultContent = `S = &empty;`
	} else {
		resultContent = `S = {${result.toString()}}`
	}

	document.getElementById("result-content").innerHTML = resultContent

}